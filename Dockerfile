# This file is a template, and might need editing before it works on your project.
FROM node:8.11

WORKDIR /usr/src/app

COPY . .

RUN npm install

# replace this with your application's default port
EXPOSE 3000
CMD [ "node", "main.js" ]
